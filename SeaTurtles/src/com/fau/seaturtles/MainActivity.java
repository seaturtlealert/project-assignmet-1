package com.fau.seaturtles;


import android.annotation.TargetApi;
import android.app.ActionBar;
import android.os.Bundle;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.NavUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;
/**
 * This is the activity that manages all of the fragments. The activity itself doesn't do much more than launch other fragments.
 * @author Mike
 * @author Andres
 *
 */
public class MainActivity extends FragmentActivity implements
		ActionBar.OnNavigationListener {

	/**
	 * The serialization (saved instance state) Bundle key representing the
	 * current dropdown position.
	 */
	private static final String STATE_SELECTED_NAVIGATION_ITEM = "selected_navigation_item";
	public static int gameScore = 0;
	public static int highScore = 0;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		// Set up the action bar to show a dropdown list.
		final ActionBar actionBar = getActionBar();
		actionBar.setDisplayShowTitleEnabled(false);
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_LIST);

		// Set up the dropdown list navigation in the action bar.
		actionBar.setListNavigationCallbacks(
		// Specify a SpinnerAdapter to populate the dropdown list.
				new ArrayAdapter<String>(getActionBarThemedContextCompat(),
						android.R.layout.simple_list_item_1,
						android.R.id.text1, new String[] {
								getString(R.string.title_section1),
								getString(R.string.title_section2),
								getString(R.string.title_section3),
								getString(R.string.title_section4),
								getString(R.string.title_section5),
								getString(R.string.title_section6),}), this);
	}

	/**
	 * Backward-compatible version of {@link ActionBar#getThemedContext()} that
	 * simply returns the {@link android.app.Activity} if
	 * <code>getThemedContext</code> is unavailable.
	 */
	@TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
	private Context getActionBarThemedContextCompat() {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
			return getActionBar().getThemedContext();
		} else {
			return this;
		}
	}

	@Override
	public void onRestoreInstanceState(Bundle savedInstanceState) {
		// Restore the previously serialized current dropdown position.
		if (savedInstanceState.containsKey(STATE_SELECTED_NAVIGATION_ITEM)) {
			getActionBar().setSelectedNavigationItem(
					savedInstanceState.getInt(STATE_SELECTED_NAVIGATION_ITEM));
		}
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		// Serialize the current dropdown position.
		outState.putInt(STATE_SELECTED_NAVIGATION_ITEM, getActionBar()
				.getSelectedNavigationIndex());
	}

	/**
	 * Shows the contents of the fragment in the container view when selected from the dropdown menu.
	 */
	@Override
	public boolean onNavigationItemSelected(int position, long id) {
		// When the given dropdown item is selected, show its contents in the
		// container view.
		if (position == 0)
		{
			//Launch intent for Identification
			Fragment fragment = new Identification();
			Bundle args = new Bundle();
			args.putInt(Identification.ARG_SECTION_NUMBER, position + 1);
			fragment.setArguments(args);
			getSupportFragmentManager().beginTransaction()
					.replace(R.id.container, fragment).commit();
			return true;
		}
		else if (position == 1)
		{
			//Show What to Do screen
			Fragment fragment = new WhatToDo();
			Bundle args = new Bundle();
			args.putInt(WhatToDo.ARG_SECTION_NUMBER, position + 1);
			fragment.setArguments(args);
			getSupportFragmentManager().beginTransaction()
					.replace(R.id.container, fragment).commit();
			return true;
		}
		else if (position == 2)
		{
			//Somehow I would like to pass the context to the map so we don't crash on the second time visiting the map. Who knows how though
			//Show map screen
			Fragment fragment = new Map();
			getSupportFragmentManager().beginTransaction()
					.replace(R.id.container, fragment).commit();
			return true;
		}
		else if (position == 3)
		{
			//Launching the game screen
			gameScore = 0;
			Fragment fragment = new Game();
			Bundle args = new Bundle();
			args.putInt(Game.ARG_SECTION_NUMBER, position + 1);
			fragment.setArguments(args);
			getSupportFragmentManager().beginTransaction()
					.replace(R.id.container, fragment).commit();
			return true;
		}
		else if (position == 4)
		{
			//Show the gumbo limbo page
			Fragment fragment = new GumboLimbo();
			Bundle args = new Bundle();
			args.putInt(Game.ARG_SECTION_NUMBER, position + 1);
			fragment.setArguments(args);
			getSupportFragmentManager().beginTransaction()
					.replace(R.id.container, fragment).commit();
			return true;
		}
		else if (position == 5)
		{
			//Show the links page
			Fragment fragment = new Links();
			Bundle args = new Bundle();
			args.putInt(Game.ARG_SECTION_NUMBER, position + 1);
			fragment.setArguments(args);
			getSupportFragmentManager().beginTransaction()
					.replace(R.id.container, fragment).commit();
			return true;
		}
		//this code should only run when we add new pages to the dropdown menu and test its functionality. It just displays the position number in a textview.
		Fragment fragment = new DummySectionFragment();
		Bundle args = new Bundle();
		args.putInt(DummySectionFragment.ARG_SECTION_NUMBER, position + 1);
		fragment.setArguments(args);
		getSupportFragmentManager().beginTransaction()
				.replace(R.id.container, fragment).commit();
		return true;
	}
	
	public int getScore() {
		return gameScore;
	}
	
	void setScore(int score) {
		gameScore = score;
	}
	
	public int getHighScore() {
		return highScore;
	}
	
	void setHighScore (int score) {
		highScore = score;
	}

	/**
	 * A dummy fragment representing a section of the app, but that simply
	 * displays dummy text.
	 */
	public static class DummySectionFragment extends Fragment {
		/**
		 * The fragment argument representing the section number for this
		 * fragment.
		 */
		public static final String ARG_SECTION_NUMBER = "section_number";

		public DummySectionFragment() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.fragment_main_dummy,
					container, false);
			TextView dummyTextView = (TextView) rootView
					.findViewById(R.id.test);
			dummyTextView.setText(Integer.toString(getArguments().getInt(
					ARG_SECTION_NUMBER)));
			return rootView;
		}
	}

	
}
