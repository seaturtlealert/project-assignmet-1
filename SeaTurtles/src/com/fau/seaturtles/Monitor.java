package com.fau.seaturtles;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;

/**
 * The monitor page, aka "How to live with turtles"
 * @author Mike
 * @author Andres
 */
public class Monitor extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_monitor);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.monitor, menu);
		return true;
	}

}
