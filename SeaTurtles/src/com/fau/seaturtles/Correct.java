package com.fau.seaturtles;

import android.os.Bundle;
import android.app.Activity;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;

/**
 * This is the screen that displays the correct answer image during the identifying game.
 * Users are linked to this screen through the Game fragment, and clicking the next button
 * will link back to the game fragment.
 * @author Mike
 * @return rootView. This is the view inflated from the layout file activity_correct
 *
 */
public class Correct extends Fragment {
	Button nextBtn;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.activity_correct,
				container, false);
		nextBtn = (Button) rootView.findViewById(R.id.next);
		nextBtn.setOnClickListener(next);
		return rootView;
	}
	/**
	 * The on click listener for the next button. Clicking this button returns the user to the Game fragment. This effectively shows the next question.
	 */
	final OnClickListener next = new OnClickListener() {
		public void onClick(final View v) {
			Fragment fragment = new Game();
			getFragmentManager().beginTransaction()
					.replace(R.id.container, fragment).commit();
		}
	};
	
}
