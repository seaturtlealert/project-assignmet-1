package com.fau.seaturtles;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
/**
 * This is the what to do screen. The screen has five buttons linking to other pages. The pages it links to is are the monitor, nesting, hatching, recycling, and five simple things.
 * @author Andres
 *
 */
public class WhatToDo extends Fragment {

	public static final String ARG_SECTION_NUMBER = "section_number";
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.activity_what_to_do,
				container, false);


	    Button monitorButton = (Button) rootView.findViewById(R.id.monitorButton);
	    monitorButton.setOnClickListener(new View.OnClickListener() {
	      @Override
	      public void onClick(View v) {
	        //updateDetail();
	    	  Intent monitor = new Intent(getActivity(), Monitor.class);
	    	  startActivity (monitor);
	      }
	    });
	    
	    Button nestingButton = (Button) rootView.findViewById(R.id.nestingButton);
	    nestingButton.setOnClickListener(new View.OnClickListener() {
	      @Override
	      public void onClick(View v) {
	        //updateDetail();
	    	  Intent nesting = new Intent(getActivity(), Nesting.class);
	    	  startActivity (nesting);
	      }
	    });	  
	    
	    Button hatchingButton = (Button) rootView.findViewById(R.id.hatchingButton);
	    hatchingButton.setOnClickListener(new View.OnClickListener() {
	      @Override
	      public void onClick(View v) {
	        //updateDetail();
	    	  Intent hatching = new Intent(getActivity(), Hatching.class);
	    	  startActivity (hatching);
	      }
	    });
	    
	    Button simpleThingsButton = (Button) rootView.findViewById(R.id.fiveSimpleThings);
	    simpleThingsButton.setOnClickListener(new View.OnClickListener() {
	      @Override
	      public void onClick(View v) {
	        //updateDetail();
	    	  Intent simpleThings = new Intent(getActivity(), FiveSimpleThings.class);
	    	  startActivity (simpleThings);
	      }
	    });
	    
	    Button recycleButton = (Button) rootView.findViewById(R.id.recycleButton);
	    recycleButton.setOnClickListener(new View.OnClickListener() {
	      @Override
	      public void onClick(View v) {
	        //updateDetail();
	    	  Intent simpleThings = new Intent(getActivity(), Recycle.class);
	    	  startActivity (simpleThings);
	      }
	    });
		return rootView;
	}
}