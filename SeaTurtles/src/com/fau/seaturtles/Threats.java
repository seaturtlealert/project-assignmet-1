package com.fau.seaturtles;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
/**
 * Threats activity using activity_threats XML file.
 * @author Mike
 * @author Andres
 */
public class Threats extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_threats);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.threats, menu);
		return true;
	}

}
