package com.fau.seaturtles;

import android.os.Bundle;
import android.app.Activity;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
/**
 * Probably the simplest page in the app. This page just contains links and information provided by Lisa. This is all done trough the activity_links xml. 
 * @author Mike
 *
 */
public class Links extends Fragment {

public static final String ARG_SECTION_NUMBER = "section_number";
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.activity_links,
				container, false);
		return rootView;
	}

}
