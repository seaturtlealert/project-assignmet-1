package com.fau.seaturtles;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
/**
 * An activity with only text at this point. This page has information about the Five Simple Things everyone can do to help sea turtles.
 * @author Mike
 *
 */
public class FiveSimpleThings extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_five_simple_things);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.five_simple_things, menu);
		return true;
	}

}
