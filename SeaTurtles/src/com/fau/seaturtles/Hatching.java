package com.fau.seaturtles;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
/**
 * Hatching activity has its content displayed entirely though XML. It displays the activity_hatching layout.
 * @author Mike
 * @author Andres
 *
 */
public class Hatching extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_hatching);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.hatching, menu);
		return true;
	}

}
