package com.fau.seaturtles;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.app.Dialog;
import android.content.Context;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.support.v4.app.FragmentActivity;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemSelectedListener;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.GoogleMap.OnMarkerClickListener;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import static com.google.android.gms.maps.GoogleMap.MAP_TYPE_HYBRID;
import static com.google.android.gms.maps.GoogleMap.MAP_TYPE_NORMAL;
import static com.google.android.gms.maps.GoogleMap.MAP_TYPE_SATELLITE;
import static com.google.android.gms.maps.GoogleMap.MAP_TYPE_TERRAIN;
/**
 * 
 * @author Andres
 *
 */
public class Map extends Fragment implements LocationListener, OnItemSelectedListener {

    GoogleMap googleMap;
    
   

    private static final LatLng CENTER = new LatLng(25.0, -75.3);
    private static final LatLng GUMBO = new LatLng(26.3666,-80.0702);
    private static final LatLng WASH = new LatLng(38.88457,-77.00572);
    private static final LatLng STRA = new LatLng(31.68371,-81.15855);
    private static final LatLng TALL = new LatLng(30.47939,-84.23472);
    private static final LatLng ARCH = new LatLng(27.81837,-80.42659);
    private static final LatLng NASSAU = new LatLng(25.00653,-77.54474);
    private static final LatLng CUBA = new LatLng(21.01555,-80.02868);
    private static final LatLng LAGG = new LatLng(27.73818,-80.38264);
    private static final LatLng BERM = new LatLng(32.30279,-64.76063);
    private static final LatLng NEVIS = new LatLng(17.29491,-62.71168);
    private static final LatLng CARI = new LatLng(14.53772,-74.98341);
    private static final LatLng SALV = new LatLng(13.65332,-88.87013);
    private static final LatLng TORTU = new LatLng(10.40339,-83.42091);
    private static final LatLng GOLFO = new LatLng(9.04174,-81.72077);
    private static final LatLng BARRI = new LatLng(27.90943,-80.47465);
    private static final LatLng GAIN = new LatLng(29.68880,-82.32874);
    private static final LatLng MIA = new LatLng(25.21566,-80.81263);
    private static final LatLng BERM1 = new LatLng(26.26956,-62.16334);
    private static final LatLng LUCIA = new LatLng(13.79090,-64.97584);
    private static final LatLng TORTU1 = new LatLng(10.56831,-83.51252);
    private static final LatLng SARA = new LatLng(27.39204,-82.51551);
    
    View rootView;
    /**
     * It creates the Map fragment and will also verify the connection with google play services
     */
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		rootView = inflater.inflate(R.layout.activity_map,
				container, false);
		googleMap = ((SupportMapFragment) getFragmentManager().findFragmentById(R.id.map))
	            .getMap();
				
		Spinner spinner = (Spinner) rootView.findViewById(R.id.layers_spinner);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(
                this.getActivity(), R.array.layers_array, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(this);
        
        // Getting Google Play availability status
        int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getActivity().getBaseContext());
 
        // Showing status
        if(status!=ConnectionResult.SUCCESS){ // Google Play Services are not available
 
            int requestCode = 10;
            Dialog dialog = GooglePlayServicesUtil.getErrorDialog(status, this.getActivity(), requestCode);
            dialog.show();
 
        }else { // Google Play Services are available
 
            // Getting reference to the SupportMapFragment of activity_main.xml
            SupportMapFragment fm = (SupportMapFragment) getFragmentManager().findFragmentById(R.id.map);
 
            // Getting GoogleMap object from the fragment
            googleMap = fm.getMap();
            
            // Enabling MyLocation Layer of Google Map
            googleMap.setMyLocationEnabled(true);
            
            //mTrafficCheckbox = (CheckBox) rootView.findViewById(R.id.traffic);
 
            // Getting LocationManager object from System Service LOCATION_SERVICE
            LocationManager locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
 
            // Creating a criteria object to retrieve provider
            Criteria criteria = new Criteria();
 
            // Getting the name of the best provider
            String provider = locationManager.getBestProvider(criteria, true);
 
            // Getting Current Location
            Location location = locationManager.getLastKnownLocation(provider);
 
            if(location!=null){
                onLocationChanged(location);
            }
            locationManager.requestLocationUpdates(provider, 300000, 0, this);
        }
        
        return rootView;
    }
   /**
    * CheckReady is making sure that the map is ready, if not it will prompt the user with a message
    * @return true or false
    */
    private boolean checkReady() {
        if (googleMap == null) {
            Toast.makeText(this.getActivity(), R.string.map_not_ready, Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }
  /**
   * It updates the current location of the user 
   */
    @Override
    public void onLocationChanged(Location location) {
 
        TextView tvLocation = (TextView) rootView.findViewById(R.id.tv_location);
 
        // Getting latitude of the current location
        double latitude = location.getLatitude();
 
        // Getting longitude of the current location
        double longitude = location.getLongitude();
 
        // Creating a LatLng object for the current location
        LatLng latLng = new LatLng(latitude, longitude);
 
        // Showing the current location in Google Map
        googleMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
 
        // Zoom in the Google Map
        googleMap.animateCamera(CameraUpdateFactory.zoomTo(15));
 
        // Setting latitude and longitude in the TextView tv_location
        tvLocation.setText("Latitude:" +  latitude  + ", Longitude:"+ longitude );
 
    }
 
    @Override
    public void onProviderDisabled(String provider) {
        // TODO Auto-generated method stub
    }
 
    @Override
    public void onProviderEnabled(String provider) {
        // TODO Auto-generated method stub
    }
 
    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
        // TODO Auto-generated method stub
    }
 /*
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.map, menu);
        
        return true;
}
*/
    /**
     * It locates the position of the SetLayer
     */
    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

        setLayer((String) parent.getItemAtPosition(position));
    }
/**
 * It checks that the layerName is the correct one with the option that the user picked
 * @param layerName
 */
    private void setLayer(String layerName) {
        if (!checkReady()) {
            return;
        }
        if (layerName.equals(getString(R.string.normal))) {
            googleMap.setMapType(MAP_TYPE_NORMAL);
        } else if (layerName.equals(getString(R.string.hybrid))) {
            googleMap.setMapType(MAP_TYPE_HYBRID);
		} else if (layerName.equals(getString(R.string.gumbo))) {
        	  CameraPosition cameraPosition = new CameraPosition.Builder()
              .target(GUMBO) // Sets the center of the map to
                                          // Golden Gate Bridge
              .zoom(17)                   // Sets the zoom
              .bearing(90) // Sets the orientation of the camera to east
              .tilt(30)    // Sets the tilt of the camera to 30 degrees
              .build();    // Creates a CameraPosition from the builder
          googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(
              cameraPosition));
          googleMap.addMarker(new MarkerOptions() 
          .position(GUMBO)
          .title("Gumbo Limbo Nature Park") .snippet("1801 North Ocean Boulevard " +
          		"Boca Raton, FL 33432" +
          		"Phone: 561-544-8605")
          .icon(BitmapDescriptorFactory
          .defaultMarker(BitmapDescriptorFactory.HUE_AZURE)));
          
	       } else if (layerName.equals(getString(R.string.policy))) {
	    	   	  CameraPosition cameraPosition = new CameraPosition.Builder()
	    	   	  .target(CENTER) // Sets the center of the map to
	    	   	  .zoom(4)
	              .build();    // Creates a CameraPosition from the builder
	          googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(
	              cameraPosition));
	          		   googleMap.addMarker(new MarkerOptions() 
	    	          .position(WASH)
	    	          .title("Commerical Fisheries") .snippet("Learn about STC's International Fisheries Policy")
	    	          .icon(BitmapDescriptorFactory
	    	          .defaultMarker(BitmapDescriptorFactory.HUE_YELLOW)));
	    	          googleMap.addMarker(new MarkerOptions() 
	    	          .position(STRA)
	    	          .title("Leatherback Strategy") .snippet("Learn about the Atlantic Leatherback Conservation Strategy")
	    	          .icon(BitmapDescriptorFactory
	    	          .defaultMarker(BitmapDescriptorFactory.HUE_YELLOW)));
	    	          googleMap.addMarker(new MarkerOptions() 
	    	          .position(TALL)
	    	          .title("Florida Coastal Policy") .snippet("Learn about STC's Free the Beach Campaign")
	    	          .icon(BitmapDescriptorFactory
	    	          .defaultMarker(BitmapDescriptorFactory.HUE_YELLOW)));
	    	          googleMap.addMarker(new MarkerOptions() 
	    	          .position(ARCH)
	    	          .title("Archie Carr Refuge") .snippet("Learn about the Archie Carr Refuge")
	    	          .icon(BitmapDescriptorFactory
	    	          .defaultMarker(BitmapDescriptorFactory.HUE_YELLOW)));
	    	          googleMap.addMarker(new MarkerOptions() 
	    	          .position(NASSAU)
	    	          .title("Clifton Preserve") .snippet("Learn about the Clifton Conservation Area")
	    	          .icon(BitmapDescriptorFactory
	    	          .defaultMarker(BitmapDescriptorFactory.HUE_YELLOW)));
	    	          googleMap.addMarker(new MarkerOptions() 
	    	          .position(CUBA)
	    	          .title("CITES and Sea Turtles") .snippet("Learn about CITES and Sea Turtles")
	    	          .icon(BitmapDescriptorFactory
	    	          .defaultMarker(BitmapDescriptorFactory.HUE_YELLOW)));
	    	          
	       } else if (layerName.equals(getString(R.string.education))) {
	    	   	  CameraPosition cameraPosition = new CameraPosition.Builder()
	    	   	  .target(CENTER) // Sets the center of the map to
	    	   	  .zoom(4)
	              .build();    // Creates a CameraPosition from the builder
	          googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(
	              cameraPosition));
	    	          googleMap.addMarker(new MarkerOptions() 
	    	          .position(BARRI)
	    	          .title("Barrier Island Center") .snippet("Learn about the Barrier Island Ecosystem Center")
	    	          .icon(BitmapDescriptorFactory
	    	          .defaultMarker(BitmapDescriptorFactory.HUE_CYAN)));
	    	          googleMap.addMarker(new MarkerOptions() 
	    	          .position(BERM1)
	    	          .title("Migration-Tracking") .snippet("Learn about STC's Sea Turtle Migration-Tracking")
	    	          .icon(BitmapDescriptorFactory
	    	          .defaultMarker(BitmapDescriptorFactory.HUE_CYAN)));
	    	          googleMap.addMarker(new MarkerOptions() 
	    	          .position(GAIN)
	    	          .title("Educational Programs") .snippet("Learn about STC's Educational Programs")
	    	          .icon(BitmapDescriptorFactory
	    	          .defaultMarker(BitmapDescriptorFactory.HUE_CYAN)));
	    	          googleMap.addMarker(new MarkerOptions() 
	    	          .position(SARA)
	    	          .title("NESTS Program") .snippet("Learn about the NESTS Program")
	    	          .icon(BitmapDescriptorFactory
	    	          .defaultMarker(BitmapDescriptorFactory.HUE_CYAN)));
	    	          googleMap.addMarker(new MarkerOptions() 
	    	          .position(MIA)
	    	          .title("Kids Corner") .snippet("Learn about STC's Turtle Tides")
	    	          .icon(BitmapDescriptorFactory
	    	          .defaultMarker(BitmapDescriptorFactory.HUE_CYAN)));
	    	          googleMap.addMarker(new MarkerOptions() 
	    	          .position(TORTU1)
	    	          .title("Kids and Conservation") .snippet("Learn about Kids and Sea Turtle Conservation")
	    	          .icon(BitmapDescriptorFactory
	    	          .defaultMarker(BitmapDescriptorFactory.HUE_CYAN)));
	    	          googleMap.addMarker(new MarkerOptions() 
	    	          .position(LUCIA)
	    	          .title("Tour de Turtles") .snippet("Learn about STC's Tour de Turtles")
	    	          .icon(BitmapDescriptorFactory
	    	          .defaultMarker(BitmapDescriptorFactory.HUE_CYAN)));
	    	          
	       } else if (layerName.equals(getString(R.string.research))) {
	    	   	  CameraPosition cameraPosition = new CameraPosition.Builder()
	    	   	  .target(CENTER) // Sets the center of the map to
	    	   	  .zoom(4)
	              .build();    // Creates a CameraPosition from the builder
	          googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(
	              cameraPosition));
	    	          googleMap.addMarker(new MarkerOptions() 
	    	          .position(LAGG)
	    	          .title("Loggerhead Tracking") .snippet("Learn about Archie Carr Refuge Loggerhead Tracking")
	    	          .icon(BitmapDescriptorFactory
	    	          .defaultMarker(BitmapDescriptorFactory.HUE_RED)));
	    	          googleMap.addMarker(new MarkerOptions() 
	    	          .position(BERM)
	    	          .title("Bermuda Turtle Project") .snippet("Learn about the Bermuda Turtle Project")
	    	          .icon(BitmapDescriptorFactory
	    	          .defaultMarker(BitmapDescriptorFactory.HUE_RED)));
	    	          googleMap.addMarker(new MarkerOptions() 
	    	          .position(NEVIS)
	    	          .title("Nevis Hawksbill") .snippet("Learn more...")
	    	          .icon(BitmapDescriptorFactory
	    	          .defaultMarker(BitmapDescriptorFactory.HUE_RED)));
	    	          googleMap.addMarker(new MarkerOptions() 
	    	          .position(CARI)
	    	          .title("Leatherback Tracking") .snippet("Learn about Caribbean Leatherback Conservation")
	    	          .icon(BitmapDescriptorFactory
	    	          .defaultMarker(BitmapDescriptorFactory.HUE_RED)));
	    	          googleMap.addMarker(new MarkerOptions() 
	    	          .position(SALV)
	    	          .title("Olive Ridley Tracking") .snippet("Learn about STC's Pacific Sea Turtle Tracking")
	    	          .icon(BitmapDescriptorFactory
	    	          .defaultMarker(BitmapDescriptorFactory.HUE_RED)));
	    	          googleMap.addMarker(new MarkerOptions() 
	    	          .position(TORTU)
	    	          .title("Tortuguero") .snippet("Learn about STC's Tortuguero Sea Turtle Research")
	    	          .icon(BitmapDescriptorFactory
	    	          .defaultMarker(BitmapDescriptorFactory.HUE_RED)));
	    	          googleMap.addMarker(new MarkerOptions() 
	    	          .position(GOLFO)
	    	          .title("Chiriqui Beach") .snippet("Learn about STC's Chiriqui Beach Sea Turtle Research")
	    	          .icon(BitmapDescriptorFactory
	    	          .defaultMarker(BitmapDescriptorFactory.HUE_RED)));
	       } else if (layerName.equals(getString(R.string.clear))) {
	    	   		   googleMap.clear();
        } else {
            Log.i("LDA", "Error setting layer with name " + layerName);
        }
    }
    
    @Override
    public void onNothingSelected(AdapterView<?> parent) {
        // Do nothing.
    }

	}