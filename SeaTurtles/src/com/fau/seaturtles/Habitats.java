package com.fau.seaturtles;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;

/**
 * Habitats activity is another page that uses XML for all of the content. The functions here are just default functions included with Activity classes that load the layout file activity_habitats
 * @author Mike
 *
 */
public class Habitats extends Activity {
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_habitats);
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.habitats, menu);
		return true;
	}
	
}
