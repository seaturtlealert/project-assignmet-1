package com.fau.seaturtles;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
/**
 * This page is showing all of its content through XML. The onCreateView just shows the activity_gumbo__limbo layout.
 * @author Mike
 *
 */
public class GumboLimbo extends Fragment {

		public static final String ARG_SECTION_NUMBER = "section_number";
		
		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.activity_gumbo__limbo,
					container, false);
			return rootView;
		}
	}