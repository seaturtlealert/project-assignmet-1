package com.fau.seaturtles;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
/**
 * We've removed access to this page since we haven't been using it. There wasn't much content on it before. This code could be removed but it was easier to just remove the button. 
 * @author Mike
 *
 */
public class Tracks extends Activity{

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_tracks);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.tracks, menu);
		return true;
	}

}
