package com.fau.seaturtles;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
/**
 * This is the identification screen. It has 3 buttons. Its pretty simple.
 * @author Mike
 * @author Andres
 *
 */
public class Identification extends Fragment {
/*
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_identification);
	}
	*/
	public static final String ARG_SECTION_NUMBER = "section_number";

	//public IdentificationTest() {
	//}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.activity_identification,
				container, false);
		
	    Button habitatButton = (Button) rootView.findViewById(R.id.habitatsButton);
	    habitatButton.setOnClickListener(new View.OnClickListener() {
	      @Override
	      public void onClick(View v) {
	        //updateDetail();
	    	  Intent habitats = new Intent(getActivity(), Habitats.class);
	    	  startActivity (habitats);
	      }
	    });
	    
	    /*
	     * Removed page from app
	    Button tracksButton = (Button) rootView.findViewById(R.id.tracksButton);
	    tracksButton.setOnClickListener(new View.OnClickListener() {
	      @Override
	      public void onClick(View v) {
	        //updateDetail();
	    	  Intent tracks = new Intent(getActivity(), Tracks.class);
	    	  startActivity (tracks);
	      }
	    });	  
	    */
	    
	    Button typesButton = (Button) rootView.findViewById(R.id.typesButton);
	    typesButton.setOnClickListener(new View.OnClickListener() {
	      @Override
	      public void onClick(View v) {
	        //updateDetail();
	    	  Intent types = new Intent(getActivity(), Types.class);
	    	  startActivity (types);
	      }
	    });
	    
	    Button threatsButton = (Button) rootView.findViewById(R.id.threatsButton);
	    threatsButton.setOnClickListener(new View.OnClickListener() {
	      @Override
	      public void onClick(View v) {
	        //updateDetail();
	    	  Intent threats = new Intent(getActivity(), Threats.class);
	    	  startActivity (threats);
	      }
	    });
		

		return rootView;
	}
	


/*
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.identification, menu);
		return true;
	}
	*/

}
