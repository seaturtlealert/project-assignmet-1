package com.fau.seaturtles;

import java.util.Random;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

/**
 * The game fragment displays a random image and four buttons. These buttons correspond to the answers for guessing the turtle type. Should users click the correct button, they will be taken to the Correct.java fragment. Incorrect guesses display a simple toast message.\n
 * The game could use a much better design. Currently the image names are loaded into the images array, in no particular order. They should be organized by each turtle type in the future. The buttons destermin which is correct through a series of switch statements. Future improvements could include randomizing the position of the buttons, or pulling random images from the internet.
 * @author Mike
 *
 */
public class Game extends Fragment {
	MainActivity main = new MainActivity();
	
	//public int localScore = main.gameScore;
	public static final String ARG_SECTION_NUMBER = "section_number";
	TextView scoreText;
	TextView highScoreText;
	int imageTest;
	View rootView;
	//Flag for which button is correct
	boolean btn1 = false,
			btn2 = false,
			btn3 = false,
			btn4 = false;
	//Creating four buttons for the answers
	Button greenBtn, loggerheadBtn, leatherbackBtn, hawksbillBtn;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		rootView = inflater.inflate(R.layout.activity_game,
				container, false);
		
		//Initialize Buttons
		greenBtn = (Button) rootView.findViewById(R.id.greenBtn);
		loggerheadBtn = (Button) rootView.findViewById(R.id.loggerheadBtn);
		leatherbackBtn = (Button) rootView.findViewById(R.id.leatherbackBtn);
		hawksbillBtn = (Button) rootView.findViewById(R.id.hawksbillBtn);
		
		scoreText = (TextView) rootView.findViewById(R.id.scoreText);
		scoreText.setText("Score: "+ main.getScore());
		highScoreText = (TextView) rootView.findViewById(R.id.highScore);
		highScoreText.setText("High Score: " + main.getHighScore());
		
		greenBtn.setOnClickListener(greenListener);
		loggerheadBtn.setOnClickListener(loggerheadListener);
		leatherbackBtn.setOnClickListener(leatherbackListener);
		hawksbillBtn.setOnClickListener(hawksbillListener);
		
		
		//MUST USE rootView INSTEAD OF getView()!!!!!
		//String [] images = {"greenturtle10", "hawksbill12", "loggerhead", "swimming_leatherback"};
		int [] images = {R.drawable.greenturtle10, R.drawable.hawksbill12, R.drawable.loggerhead2, R.drawable.swimming_leatherback, R.drawable.green_sea_turtle_habitat, R.drawable.hawksbill_habitat, R.drawable.leatherback_habitat, R.drawable.loggerhead, R.drawable.loggerhead_habitat};//greenturtle10, hawksbill12, loggerhead2, swimming_leatherback
		//Random r = new Random();
		//imageTest = r.nextInt(3);
		ImageView pic = (ImageView) rootView.findViewById(R.id.pic);
		imageTest = getRandomImage();
		//Setting correct answers. Very confusing
		if (imageTest == 0 || imageTest == 4)
		{
			btn1 = true;
		}
		else if (imageTest == 1 || imageTest == 5)
		{
			btn4 = true;
		}
		else if (imageTest == 2 || imageTest == 7 || imageTest == 8)
		{
			btn2 = true;
		}
		else if (imageTest == 3 || imageTest == 6)
		{
			btn3 = true;
		}
		pic.setImageResource(images[imageTest]);
		
	    return rootView;
	}
	/**
	 * This function is used to get a random number within the correct answer to use for the index of the array. It suffers from modulo bias, meaning it can be improved for a more pure form of random numbers in the future.
	 * @return A random integer value 0-8 to be used as the index for the images array
	 */
	public int getRandomImage()
	{
		int image=0;
		Random r = new Random();
		image = r.nextInt(9);
		return image;
	}
	
	/**
	 * On click listener for the green turtle button. If the button is clicked, it checks to see if this button is currently the correct answer. If it is, it displays a toast message, and launches the correct fragment. If it is incorrect, it just displays the incorrect toast message.
	 */
	final OnClickListener greenListener = new OnClickListener() {
		 public void onClick(final View v) {
	            //Inform the user the button has been clicked
	            //Toast.makeText(this, "Button1 clicked.", Toast.LENGTH_SHORT).show();
				 if (btn1 == true){
					 main.setScore(main.getScore()+1);
					 scoreText.setText("Score: "+ main.getScore());
					 if (main.getScore() >= main.getHighScore())
					 {
						 main.setHighScore(main.getScore());
						 highScoreText.setText("High Score: " + main.getHighScore());
					 }
					 Toast.makeText(getActivity(), "Correct!", Toast.LENGTH_SHORT).show();
					 Fragment fragment = new Correct();
						getFragmentManager().beginTransaction()
								.replace(R.id.container, fragment).commit();
				 }
				 else
				 {
					 main.setScore(0);
					 scoreText.setText("Score: "+ main.getScore());
					 Toast.makeText(getActivity(), "Incorrect!", Toast.LENGTH_SHORT).show();
				 }
	        }
	};
	
	/**
	 * On click listener for the loggerhead turtle button. If the button is clicked, it checks to see if this button is currently the correct answer. If it is, it displays a toast message, and launches the correct fragment. If it is incorrect, it just displays the incorrect toast message.
	 */
	final OnClickListener loggerheadListener = new OnClickListener() {
		 public void onClick(final View v) {
	            //Inform the user the button has been clicked
	            //Toast.makeText(this, "Button1 clicked.", Toast.LENGTH_SHORT).show();
				 if (btn2 == true){
					 main.setScore(main.getScore()+1);
					 scoreText.setText("Score: "+ main.getScore());
					 if (main.getScore() >= main.getHighScore())
					 {
						 main.setHighScore(main.getScore());
						 highScoreText.setText("High Score: " + main.getHighScore());
					 }
					 Toast.makeText(getActivity(), "Correct!", Toast.LENGTH_SHORT).show();
					 Fragment fragment = new Correct();
						getFragmentManager().beginTransaction()
								.replace(R.id.container, fragment).commit();
				 }
				 else
				 {
					 main.setScore(0);
					 scoreText.setText("Score: "+ main.getScore());
					 Toast.makeText(getActivity(), "Incorrect!", Toast.LENGTH_SHORT).show();
				 }
	        }
	};
	
	/**
	 * On click listener for the leatherback turtle button. If the button is clicked, it checks to see if this button is currently the correct answer. If it is, it displays a toast message, and launches the correct fragment. If it is incorrect, it just displays the incorrect toast message.
	 */
	final OnClickListener leatherbackListener = new OnClickListener() {
		 public void onClick(final View v) {
	            //Inform the user the button has been clicked
	            //Toast.makeText(this, "Button1 clicked.", Toast.LENGTH_SHORT).show();
				 if (btn3 == true){
					 main.setScore(main.getScore()+1);
					 scoreText.setText("Score: "+ main.getScore());
					 if (main.getScore() >= main.getHighScore())
					 {
						 main.setHighScore(main.getScore());
						 highScoreText.setText("High Score: " + main.getHighScore());
					 }
					 Toast.makeText(getActivity(), "Correct!", Toast.LENGTH_SHORT).show();
					 Fragment fragment = new Correct();
						getFragmentManager().beginTransaction()
								.replace(R.id.container, fragment).commit();
				 }
				 else
				 {
					 main.setScore(0);
					 scoreText.setText("Score: "+ main.getScore());
					 Toast.makeText(getActivity(), "Incorrect!", Toast.LENGTH_SHORT).show();
				 }
	        }
	};
	
	/**
	 * On click listener for the hawksbill turtle button. If the button is clicked, it checks to see if this button is currently the correct answer. If it is, it displays a toast message, and launches the correct fragment. If it is incorrect, it just displays the incorrect toast message.
	 */
	final OnClickListener hawksbillListener = new OnClickListener() {
		 public void onClick(final View v) {
	            //Inform the user the button has been clicked
	            //Toast.makeText(this, "Button1 clicked.", Toast.LENGTH_SHORT).show();
				 if (btn4 == true){
					 main.setScore(main.getScore()+1);
					 scoreText.setText("Score: "+ main.getScore());
					 if (main.getScore() >= main.getHighScore())
					 {
						 main.setHighScore(main.getScore());
						 highScoreText.setText("High Score: " + main.getHighScore());
					 }
					 Toast.makeText(getActivity(), "Correct!", Toast.LENGTH_SHORT).show();
					 Fragment fragment = new Correct();
						getFragmentManager().beginTransaction()
								.replace(R.id.container, fragment).commit();
				 }
				 else
				 {
					 main.setScore(0);
					 scoreText.setText("Score: "+ main.getScore());
					 Toast.makeText(getActivity(), "Incorrect!", Toast.LENGTH_SHORT).show();
				 }
	        }
	};
	
}
