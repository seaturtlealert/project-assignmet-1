package com.fau.seaturtles;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
/**
 * Nesting activity. Using XML layout file activity_nesting
 * @author Mike
 * @author Andres
 */
public class Nesting extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_nesting);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.nesting, menu);
		return true;
	}

}
